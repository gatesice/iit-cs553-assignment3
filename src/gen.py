import string, random
import requests
import os
import time
from threading import Thread
import sys

fsize = [1024,
         1024 * 10,
         1024 * 100,
         1024 * 1024,
         1024 * 1024 * 10,
         1024 * 1024 * 100,]

fcount = [100,
          100,
          100,
          100,
          10,
          1,]

def gen(size=100, chars=string.ascii_letters + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def genl(size=100):
    return gen(size=size-1) + '\n'

def gen_files():
    flist = []
    for i in range(0, len(fsize)):
        for j in range(0, fcount[i]):
            fname = gen(size=10)
            fp = open('../files/' + fname, mode='w')
            flist.append(fname)
            wsize = 0
            while wsize < fsize[i]:
                if fsize[i] - wsize >= 100:
                    fp.write(genl())
                    wsize += 100
                else:
                    fp.write(genl(size=fsize[i] - wsize))
                    wsize = fsize[i]
    return flist

def upload_big_file(fp, f):
    readed = 5 * 1024 * 1024
    res = requests.post('http://hw3.cs553.iit.gatesice.com/big_insert/open/' + f)
    if res.text != 'OK':
        print('cannot upload the big file')
        return
    buf = fp.read(5 * 1024 * 1024)
    readed = len(buf)
    rty = 10
    while readed == 5 * 1024 * 1024:
        try:
            print('Buffer readed, length=%d' % len(buf))
            res = requests.post('http://hw3.cs553.iit.gatesice.com/big_insert/insert/' + f, files={'uf':(f, buf, 'binary/octet-stream', {})})
            print('    buffer uploaded...')
            if res.text != 'OK':
                raise BaseException('Cannot write the file!')
            readed = len(buf)
            buf = fp.read(5 * 1024 * 1024)
        except:
            print('Now will retry...')
            rty -= 1
            if rty <= 0:
                break
            pass
    res = requests.post('http://hw3.cs553.iit.gatesice.com/big_insert/close/' + f)

    

def upload_files(tid, tt):
    print('upload all 411 files')
    flist = filter(lambda x: not x.startswith('.'), os.listdir('../files'))
    t1 = time.time()
    os.chdir('../files/')
    c = -1
    for f in flist:
        c += 1
        if c % tt != tid:
            continue
        print(str(c) + ' : uploading ' + f + ' ...')
        if os.path.getsize(f) >= 1024 * 1024 * 30:
            fp = open(f, 'rb')
            upload_big_file(fp, f)
        else:
            with open(f, 'rb') as fp:
                res = requests.post('http://hw3.cs553.iit.gatesice.com/insert', files={'uf':(f, fp, 'binary/octet-stream', {})})
                print(res)
    t2 = time.time()
    print('Thread %d takes %f seconds.' % (tid, t2 - t1))
    
def find_files(tid, tt):
    print('find all 411 files')
    flist = filter(lambda x: not x.startswith('.'), os.listdir('../files'))
    t1 = time.time()
    os.chdir('../files/')
    c = -1
    for f in flist:
        c += 1
        if c % tt != tid:
            continue
        print(str(c) + 'finding ' + f + '...')
        res = requests.get('http://hw3.cs553.iit.gatesice.com/find/%s' % f)
    t2 = time.time()
    print('Thread %d takes %f seconds.' % (tid, t2 - t1))
    
def remove_files(tid, tt):
    print('remove all 411 files')
    flist = filter(lambda x: not x.startswith('.'), os.listdir('../files'))
    t1 = time.time()
    os.chdir('../files/')
    c = -1
    for f in flist:
        c += 1
        if c % tt != tid:
            continue
        print(str(c) + 'removing ' + f + '...')
        res = requests.post('http://hw3.cs553.iit.gatesice.com/remove/%s' % f)
    t2 = time.time()
    print('Thread %d takes %f seconds.' % (tid, t2 - t1))

if __name__ == '__main__':
    if len(sys.argv) < 4:
        print('You need 3 arguments: MEMCACHE(on|off) OPERATION(insert|find|remove) MULTITHREAD(yes|no) or use genfile as first argument to generate files.')
    if sys.argv[1] == 'genfile':
        gen_files()
        exit()
    if sys.argv[1] == 'off':
        requests.get('http://hw3.cs553.iit.gatesice.com/memcache/off')
        print('Memcache turned off')
    elif sys.argv[1] == 'on':
        requests.get('http://hw3.cs553.iit.gatesice.com/memcache/on')
        print('Memcache turned on')
    else:
        raise BaseException('First arguemnt must be on/off for memcache.')
    
    if sys.argv[2] == 'insert':
        if sys.argv[3] == 'no':
            print('Starting upload files with 1 thread.')
            upload_files(0, 1)
        elif sys.argv[3] == 'yes':
            print('Starting upload files with 4 threads.')
            ts = []
            for i in range(0, 4):
                _t = Thread(target=upload_files, args=(i, 4))
                _t.start()
                ts.append(_t)
            for t in ts:
                t.join()
    elif sys.argv[2] == 'find':
        if sys.argv[3] == 'no':
            print('Starting find files with 1 thread.')
        elif sys.argv[3] == 'yes':
            find_files(0, 1)
            print('Starting find files with 4 threads.')
            ts = []
            for i in range(0, 4):
                _t = Thread(target=find_files, args=(i, 4))
                _t.start()
                ts.append(_t)
            for t in ts:
                t.join()        
    elif sys.argv[2] == 'remove':
        if sys.argv[3] == 'no':
            print('Starting remove files with 1 threa.')
            remove_files(0, 1)
        elif sys.argv[3] == 'yes':
            print('Starting remove files with 4 threads.')
            ts = []
            for i in range(0, 4):
                _t = Thread(target=remove_files, args=(i, 4))
                _t.start()
                ts.append(_t)
            for t in ts:
                t.join()        
        
        
        