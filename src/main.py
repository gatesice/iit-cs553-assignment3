from flask import Flask, request, session, g, redirect, url_for, \
     abort, render_template, flash, make_response
app = Flask(__name__)
app.config['DEBUG'] = True

from google.appengine.api import memcache

from google.appengine.ext import blobstore
from google.appengine.api.files import blobstore as bs
import google.appengine.api.files as files
import cloudstorage as gcs

import werkzeug




gcs_file = {}
mem_file = {}
msg = None
inited = False
do_memcache = True




@app.route('/memcache/off')
def memcache_off_web():
    memcache_off()
    return redirect(url_for('home'))
def memcache_off():
    global do_memcache
    do_memcache = False    
    
@app.route('/memcache/on')
def memcache_on_web():
    memcache_on()
    return redirect(url_for('home'))
def memcache_on():
    global do_memcache
    do_memcache = True    
    



def set_session_msg(_m):
    global msg
    msg = _m




@app.route('/removeall')
def remove_all_WEB():
    remove_all_cache()
    remove_all_gcs_file()
    set_session_msg('All file and cache removed!')
    return redirect(url_for('home'))

def remove_all_gcs_file():
    l = gcs.listbucket('/mem-cache.appspot.com/')
    for f in l:
        gcs.delete(f.filename)
        



@app.route('/removeallcache')
def remove_all_cache_WEB():
    remove_all_cache()
    set_session_msg('All cache removed!')
    return redirect(url_for('home'))

def remove_all_cache():
    memcache.flush_all()




@app.route('/checkcache', methods=['GET'])
def checkCache_WEB():
    if request.method == 'GET':
        if 'val' in request.args:
            if checkCache(request.args['val']):
                set_session_msg('checkCache; %s; TRUE' % request.args['val'])
                return redirect(url_for('home'))
    set_session_msg('checkCache; FALSE')
    return redirect(url_for('home'))

def checkCache(filename):
    val = memcache.get(filename)
    return val != None


    
    
@app.route('/')
def home():
    """
    Homepage.
    """
    global inited
    inited = True
    session.files = []
    #session.upload_url = 'https://www.googleapis.com/sotrage/v1/b?project=AIzaSyC0LV-fZBsmty-Iyc_X3kygboWE75ndLAU'
    session.msg = '' if msg == None else msg
    #set_session_msg(None)
        
    session.upload_url = '/insert'
    session.nofiles = True
    #print('memcache:', '\t'.join(mem_file.keys()))
    print('show message: %s' % msg)
    return render_template('index.html')




@app.errorhandler(405)
def method_not_allowed(e):
    """Return a custom 405 error."""
    return redirect(url_for('home'))




@app.errorhandler(404)
def page_not_found(e):
    """Return a custom 404 error."""
    return redirect(url_for('home'))




@app.route('/insert', methods=['POST'])
def insert():
    if request.method == 'POST':
        print(request.files['uf'])
        uf = request.files['uf']
        if uf.filename == None or uf.filename == '':
            return redirect(url_for('home'))
        fname = '/mem-cache.appspot.com/%s' % uf.filename
        f = uf.stream.read()
        with gcs.open(fname, mode='w') as gcs_file:
            gcs_file.write(f)
        if len(f) <= 100 * 1024 and do_memcache:
            uf.stream.seek(0)
            #mem_file[uf.filename] = f
            memcache.set(key=uf.filename, value=f, time=3600)
        print("""
upload-file: %s
content-length: %d
memcache-list:
        """ %  (uf.filename, len(f)))
        
    return redirect(url_for('home'))




@app.route('/big_insert/open/<filename>', methods=['POST'])
def big_insert_open(filename):
    """
    Open a file handler for uploading big file.
    This request won't create 
    """
    global gcs_file
    if request.method == 'POST':
        if filename not in gcs_file or gcs_file[filename] != None:
            gcs_file[filename] = gcs.open(('/mem-cache.appspot.com/%s' % filename), mode='w')
        else:
            return "Can't open the file handler in GCS", 400
    return 'OK'



            
@app.route('/big_insert/insert/<filename>', methods=['POST'])
def big_insert(filename):
    """
    Write files into the given file. 
    The file should opened by ``POST /big_insert/open/<filename>``
    """
    if request.method == 'POST':
        if filename not in gcs_file or gcs_file[filename] == None:
            return "You didn't open the file yet", 400
        uf = request.files['uf']
        if uf.filename == None or uf.filename == '':
            return
        gcs_file[filename].write(uf.stream.read())
        gcs_file[filename].flush()
    return 'OK'




@app.route('/big_insert/close/<filename>', methods=['POST'])
def big_insert_close(filename):
    """
    Close the specific file handler.
    """
    if request.method == 'POST':
        if filename not in gcs_file or gcs_file[filename] == None:
            return 'Not found', 400
        gcs_file[filename].close()
        gcs_file[filename] = None
    return 'OK'




@app.route('/remove/<filename>', methods=['POST', 'GET'])
def remove(filename):
    if request.method in ['POST', 'GET']:
        x = True
        f = memcache.get(filename)
        if f is not None:
            memcache.delete(filename)
            x = False
            
        try:
            gcs.delete(('/mem-cache.appspot.com/%s' % filename))
            x = False
        except: pass
        
        if x:
            set_session_msg('File: %s ; NOT FOUND!!' % filename)
        else:
            set_session_msg('File: %s ; deleted!' % filename)
        return redirect(url_for('home'))
            
    set_session_msg('Method error! Check your source code!')
    return redirect(url_for('home'))




@app.route('/remove', methods=['POST'])
def remove_btn():
    if request.method == 'POST':
        if 'df' in request.form:
            return redirect(url_for('remove_btn') + ('/%s' % request.form['df']), code=307)
    set_session_msg('Method error! Check your source code!')
    return redirect(url_for('home'))




@app.route('/find/<filename>', methods=['GET'])
def find(filename):
    if request.method == 'GET':
        response = None
        f = memcache.get(filename)
        if f is not None:
            return f
        else:
            try:
                with gcs.open(('/mem-cache.appspot.com/%s' % filename), 'r') as fp:
                    return fp.read()
            except:
                pass
        
    set_session_msg('Method error! Check your source code!')
    return redirect(url_for('home'))    
    
    
    
    

@app.route('/find', methods=['GET'])
def find_btn():
    if request.method == 'GET':
        print(request.args)
        if 'ff' in request.args:
            return redirect(url_for('find_btn') + ('/%s' % request.args['ff']))
    set_session_msg('Method error! Check your source code!')
    return redirect(url_for('home'))




@app.route('/check/<filename>', methods=['GET'])
def check(filename):
    if request.method == 'GET':
        not_in_memcache = True
        msg = 'Filename: %s;' % filename
        x = True
        
        f = memcache.get(filename)
        if f is not None:
            msg += 'In memcache; '
            x = False
            
        try:
            fs = gcs.stat(('/mem-cache.appspot.com/%s' % filename))
            msg += 'In google cloud storage. '
            x = False
        except:
            if not_in_memcache:
                set_session_msg('The file %s does not exists in server!' % filename)
                
        if x: msg += 'Not Exists!'
        set_session_msg(msg)
        return redirect(url_for('home'))            
    set_session_msg('Method error! Check your source code!')
    return redirect(url_for('home'))




@app.route('/check', methods=['GET'])
def check_btn():
    if request.method == 'GET':
        print(request.args)
        if 'cf' in request.args:
            return redirect(url_for('check_btn') + ('/%s' % request.args['cf']))
    set_session_msg('Method error! Check your source code!')
    return redirect(url_for('home'))




@app.route('/filelist', methods=['POST', 'GET'])
def listfile():
    flist = []
    ls = gcs.listbucket('/mem-cache.appspot.com')
    for f in ls:
        obj = {}
        fn = f.filename.split('/')[-1]
        obj['name'] = fn
        obj['uri_name'] = werkzeug.urls.iri_to_uri(fn)
        obj['size_string'] = str(f.st_size) + ' Bytes'
        x = memcache.get(fn)
        #obj['cached'] = '' if x == None else 'cached'
        obj['hidden'] = 'display:none' if x == None else ''
        flist.append(obj)
    session.files = flist
    return render_template('list.html')
        