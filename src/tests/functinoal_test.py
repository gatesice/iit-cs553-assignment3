import nose
    
def experiment_memcache_on_insert():
    import requests
    from threading import Thread
    from gen import *
    
    requests.get('http://hw3.cs553.iit.gatesice.com/memcache/on')
    
    print('Starting upload files with 1 thread.')
    upload_files(0, 1)
    print('Starting upload files with 4 threads.')
    ts = []
    for i in range(0, 4):
        _t = Thread(target=upload_files, args=(i, 4))
        ts.append(_t)
    for t in ts:
        t.join()
        
def experiment_memcache_off_insert():
    import requests
    from threading import Thread
    from gen import *
    
    requests.get('http://hw3.cs553.iit.gatesice.com/memcache/off')
    
    print('Starting upload files with 1 thread.')
    upload_files(0, 1)
    print('Starting upload files with 4 threads.')
    ts = []
    for i in range(0, 4):
        _t = Thread(target=upload_files, args=(i, 4))
        ts.append(_t)
    for t in ts:
        t.join()    
        
        
def experiment_memcache_on_find():
    import requests
    from threading import Thread
    from gen import *
    
    requests.get('http://hw3.cs553.iit.gatesice.com/memcache/on')
    print('Starting find files with 1 thread.')
    find_files(0, 1)
    print('Starting find files with 4 threads.')
    ts = []
    for i in range(0, 4):
        _t = Thread(target=find_files, args=(i, 4))
        ts.append(_t)
    for t in ts:
        t.join()       
        
        
def experiment_memcache_off_find():
    import requests
    from threading import Thread
    from gen import *
    
    requests.get('http://hw3.cs553.iit.gatesice.com/memcache/off')
    print('Starting find files with 1 thread.')
    find_files(0, 1)
    print('Starting find files with 4 threads.')
    ts = []
    for i in range(0, 4):
        _t = Thread(target=find_files, args=(i, 4))
        ts.append(_t)
    for t in ts:
        t.join()       
        
def experiment_memcache_on_remove():
    import requests
    from threading import Thread
    from gen import *
    
    requests.get('http://hw3.cs553.iit.gatesice.com/memcache/on')
    print('Starting remove files with 1 threa.')
    remove_files(0, 1)
    print('Starting remove files with 4 threads.')
    ts = []
    for i in range(0, 4):
        _t = Thread(target=remove_files, args=(i, 4))
        ts.append(_t)
    for t in ts:
        t.join()           
        
def experiment_memcache_off_remove():
    import requests
    from threading import Thread
    from gen import *
    
    requests.get('http://hw3.cs553.iit.gatesice.com/memcache/off')
    print('Starting remove files with 1 threa.')
    remove_files(0, 1)
    print('Starting remove files with 4 threads.')
    ts = []
    for i in range(0, 4):
        _t = Thread(target=remove_files, args=(i, 4))
        ts.append(_t)
    for t in ts:
        t.join()           
    
def test_gen_file():
    from gen import *
    gen_files()